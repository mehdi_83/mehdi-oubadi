<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<title>produit</title>
	<style>
		table,
		tr,
		td,
		th {
			border: 2px solid lightcoral;
			border-collapse: collapse;
		}

		td,
		th {
			padding: 20px 30px;
		}
	</style>
</head>

<body>
	<h1>liste des produits</h1>
	<table>
		<tr>
			<th>ID</th>
			<th>Designation</th>
			<th>Prix unitaire</th>
			<th>Action</th>
		</tr>
		<?php foreach ($data as $key => $value) : ?>
			<tr>
				<td><?= $value["id"] ?></td>
				<td><?= $value["designation"] ?></td>
				<td><?= $value["prix_unitaire"] ?></td>
				<td><a href="/produit/<?= $value["id"] ?>">supprimer</a></td>
			</tr>
		<?php endforeach; ?>
	</table>

	<script>
		$(function() {

			$("a").click(function(event) {
				// console.log(event);
				event.preventDefault()
				let tr = this.parentElement.parentElement
				id = tr.firstElementChild.textContent;
				$.ajax({
						type: "DELETE",
						url: `/produit/${id}`
					})
					.done(function(msg) {
						tr.remove();
					});

			})
			// function supprimer(event, id) {
			// }

		});
	</script>
</body>

</html>